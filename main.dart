import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<Weather> fetchWeather() async {

  final response = await http.get('http://192.168.1.100:1323?cityName=colombo');

print(response.body);

    return Weather.fromJson(json.decode(response.body));

}

class Weather {
  final String desc;
  final double temp;
  final double pressure;
  final double humidity;

  Weather({this.desc,this.temp, this.pressure, this.humidity});

  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
      desc:json['weather']['description'],
      temp: json['main']['temp'],
      pressure: json['main']['pressure'],
      humidity: json['main']['humidity'],
    );
  }
}

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future<Weather> futureWeather;

  @override
  void initState() {
    super.initState();
    futureWeather = fetchWeather();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Weather data',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fetch Weather data'),
        ),
        body: Center(

          child: FutureBuilder<Weather>(

            future: futureWeather,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text(snapshot.data.desc);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return CircularProgressIndicator();

            },
          ),
        ),
      ),
    );
  }
}
